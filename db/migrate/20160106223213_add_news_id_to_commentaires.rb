class AddNewsIdToCommentaires < ActiveRecord::Migration
  def change
  	add_column :commentaires, :news_id, :integer
  	add_index :commentaires, :news_id  	
  end
end
