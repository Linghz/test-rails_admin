class TableCommentaires < ActiveRecord::Migration
  def change
  	create_table :commentaires
  	add_column :commentaires, :commentaire, :text
  	add_column :commentaires, :created_at, :datetime

  end
end
